FTP-DAK-SETUP(7)
================
:doctype: manpage


NAME
----
ftp-dak-setup - How to set up a standard dak process-new workflow.

INTRO
-----

Welcome :)

This is the first in a series of ftp-master manpages which help describe
common rules and procedures. You can get the full source at
ftp-master.debian.org:~paultag/git/rnp.git 

Send patches, corrections or additions to paultag@debian.org.

WORKFLOW
--------

It's very common for the ftpteam to use two ssh connections, one for *mc(1)*,
and another to invoke *dak(1)*. This workflow assumes you use tmux.


SETTING UP YOUR BASHRC
----------------------

Add the following helpful lines to your bashrc.

    export DAK_INSPECT_UPLOAD='tmux new-session -d -s process-new 2>/dev/null; tmux new-window -n "{changes}" -t process-new:^ -k "cd {directory}; mc"'
    # This will push dak to alter your process-new mc session session. This
    # is the most important one.

The following helpful commands may help you work in NEW:

    alias x='dpkg-source -x *dsc'
    alias lc='licensecheck --recursive . | sed s/.\*:\ //g | sort | uniq -c'
    alias copyright='grep -ir copyright *|less'
    alias tdak='tmux attach -t dak'
    alias tpn='tmux attach -t process-new'

WAYS TO USE MC FOR THE NON-MCITE
--------------------------------

   1) If you do C-c, q - mc will show you a quick view.
   2) C-o will give you the cwd (process-new directory)

USING THE GANNEFF DIRECTORY HACK
--------------------------------

Create the extension file:

   1) Open mc

   2) Command => Edit extension file

   3) Close the file. (this just creates the file for us)

   4) Quit mc

Load the ganneff plugin:

   1) mkdir -p ~/.local/share/mc/extfs.d

   2) cp ~paultag/HACKING/mc-setup/extfs.ganneff \
        ~paultag/HACKING/mc-setup/extfs.gannefffast \
        ~/.local/share/mc/extfs.d/ganneff

   3) chmod +x ~/.local/share/mc/extfs.d/ganneff \
        ~/.local/share/mc/extfs.d/gannefffast

Enable the ganneff plugin:

   1) ${EDITOR} ~/.config/mc/mc.ext

   2) replace entries you wish to flat-file with:

            Open=%cd %p/gannefffast://

      (gannefffast extracts before, ganneff just reads the tar listing)

   3) The supported types are: lzma, xz, bz2, gz, z, Z, feel free to set them
      all to gannefffast.


AUTHOR
------
This helpful guide for hackers was written by
Paul R. Tagliamonte <paultag@debian.org> for the FTP-Master team and all
future ftp hackers that come around.


RESOURCES
---------

*dak(1)*, *tmux(1)*, *mc(1)*


COPYING
-------
Copyright (C) 2013 Paul Tagliamonte. This file, and others in this package
may be distributed under the terms of the MIT/Expat license. A copy of which
may be found in the source distribution.
