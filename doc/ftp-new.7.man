FTP-NEW(7)
==========
:doctype: manpage


NAME
----
ftp-new - Rules and Procedures when processing the NEW queue as part of the
          ftp-team.

INTRO
-----

Welcome to NEW. NEW is a special sort of hell, where good developers
come to smash their head against the waves of incoming evil, such as
license violations, binary baddies or typos made because double-checking
is just too much work. I mean, it installed, right?

RULES
-----

You, as the FTP-Master, must *never* allow *any* of this into the archive.
You are a stop-gap. You are the last line of defense. You are what stands
between Debian and hordes of lawyers, keen to sue Debian for all we're
worth.

  1. Never, ever, ever let anything questionable into NEW. It's easier to
     *not* process such things, and talk with the rest of the team, then it
     is to heard DDs into fixing the mistake.

  2. *NEVER* take *ANY* files off ftp-master, *EVER*. You may examine them
     on the server, but *NEVER* take them off. For *ANY* reason. This point
     may not be made too clearly.

  3. Go *carefully* through the *entire* source tree. Move slowly, this work
     takes time. It's a lot easier to spend 5 extra minutes now than it is
     to deal with a mess made due to not being careful.

  4. Check packages for idiomatic names to the teams to which they belong,
     and don't introduce something stupid™ into the archive. Again, we're
     a last resort. Don't let anything past.

  5. Uploads that are in binary-NEW must be reviewed just as throughly
     as uploads that introduce new source packages.

  6. This should go without saying, but *don't* process uploads that
     you yourself signed, i.e., your own uploads, and uploads you've
     sponsored.

PROCEDURES
----------

This section will outline a few common procedures when in NEW.


BASIC WORKFLOW
~~~~~~~~~~~~~~

Before you start, please review the setup guide to configure dak to properly
create a *tmux(1)* session for you, and set up *mc(1)* for ftp-reviewing.

  1. ssh into ftp-master (two ssh connections is helpful)
  2. in the first, run *dak process-new*.
  3. in the second, run *tmux attach*.
  5. review basic package details (hit *c* in the dak window).
  6. review every file via the *mc(1)* pane, using C-x, q may be
     helpful. Every single package has issues, your job is to find them.
  7. If the package has minor issues, you may consider proding the maintainer
     to address the issue in future uploads. Grave issues get a reject, and
     good packages get the accept.
  8. Repeat until blunt force trauma forces you to pass out.


I FOUND A BINARY IN THE SOURCE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Congrats! You were paying attention! What happens to the package is usually
up to you, the ftp-master, and how the binary is being used.

  * If the prebuilt binary exists in the binary package as well as the source
    package, you should *REJECT* this package with a note explaining that we
    need to rebuild all binaries from source. If the package is currently in
    the archive, please *file a RC bug with serious severity on the package*.

  * If the prebuilt binary exists in the source package, but a rebuilt binary
    is getting shipped in the binary package, we may be able to accept the 
    package, providing we can be assured of the following:

      1. Can you ensure that it *can* be rebuilt?
      2. Can you ensure that the binary in the .deb *was* rebuilt?
      3. Can you ensure it complies with the licensing of the binary?


I NEED TO PROCESS A SINGLE PACKAGE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since doing a bunch of skips through the queue is painful, you can
use the following hack to process a single package out of band, due
to a pressing need for the package in unstable.

  1. ssh into ftp-master
  2. cd /srv/ftp-master.debian.org/queue/new/
  3. dak process-new package*changes

    * It's absolutely critical that you add the wildcard to this command,
      or you might miss versions that were uploaded while the package was
      in NEW.

  4. process normally (see above)

HOW TO REPLY TO A PROD / REJECT REPLY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When answering questions regarding a *REJECT* or a *PROD*, it's common to
reply to the reply (heh) using your @debian.org email, while *always* keeping
ftpmaster@ftp-master.debian.org on CC.

Please be nice.


I NEED TO REJECT A PACKAGE BUT ACCEPT ANOTHER VERSION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This may happen if a non-DFSG free source tarball was uploaded, and latter
a DFSG tarball was uploaded, without rejecting the first one.

  1. ssh into ftp-master
  2. cd /srv/ftp-master.debian.org/queue/new/
  3. dak process-new package_bad-version*changes
  4. Mark for *REJECT*
  5. dak process-new package_good-version*changes
  6. Mark for *ACCEPT*


OH SHIT, I NEED TO UNACCEPT SOMETHING QUICKLY
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

OK. Keep cool. Make sure that dak hasn't already processed this yet. Look for
the NEW and BYHAND processing email. If you've not got it yet, there might
be enough time to check.

Dak looks in /srv/ftp-master.debian.org/queue/new/COMMENTS/ for the hints,
so, let's move the hint out of the way before dak gets there.

*DO NOT DO THIS UNLESS YOU REALLY NEED TO* - *DON'T BE SO CARELESS ACCEPTING
PACKAGES*

    cd /srv/ftp-master.debian.org/queue/new/COMMENTS/
    mv ACCEPTED.python-schroot_0.3-1_amd64 ~
    # OK. Now you have time. If this needs to stay around, please
    # leave a dak note on the package.


HACKS
-----

In addition to doing things the right way, the following fun hacks are
handy for aiding with the review of the package.

THE SCOTTK GREP HACK
~~~~~~~~~~~~~~~~~~~~

When reviewing the package, it may be handy to do a quick grep for
the string "copyright", which can pull it out of some funny places.

  grep -Eir '(copyright|©)' *|less

Over the source.

THE JWILK CODE REVIEWER
~~~~~~~~~~~~~~~~~~~~~~~

When reviewing the package, it may be handy to check over the source
for anything that "looks" funny to you. Jakub Wilk gave me a handy
hack for doing normal package reviews that's handy in ftp-land too.

  alias code='find -type f -exec head -c 1T {} + > ../code && vim ../code'

Be sure to thank him!


THE PAULTAG COPYRIGHT-O-MATIC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Checking for license incompatibilities can also be handy. Using
*licensecheck(1)* (which *SHOULD NOT BE TRUSTED*) can be helpful for
a first pass.

    licensecheck --recursive . | sed s/.\*:\ //g | sort | uniq -c

Do review everything manually too.


REVIEWING FILES WITH NON-ASCII NAMES
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The mc/gannefffast setup struggles with these, so you can review them
separately like this:

    LC_ALL=C find . -name '*[! -~]*' -exec env LC_ALL=C.UTF-8 less -f '{}' \;


AUTHOR
------
This helpful guide for hackers was originally written by
Paul R. Tagliamonte <paultag@debian.org> for the FTP-Master team and all
future ftp hackers that come around.  Subsequent to its initial
creation, others have made contributions; see the list of copyright
notices below.


RESOURCES
---------

*dak(1)*, *tmux(1)*, *mc(1)*


COPYING
-------
Copyright (C) 2013      Paul Tagliamonte

Copyright (C) 2019-2020 Sean Whitton

This file, and others in this package may be distributed under the
terms of the MIT/Expat license, a copy of which may be found in the
source distribution.
